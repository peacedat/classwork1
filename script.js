$(document).ready(async function(){
    const slider = $('.slider').slick({
        centerMode: true,
        centerPadding: '60px',
        slidesToShow: 3,

    });


const {data: photos} = await axios.get('https://jsonplaceholder.typicode.com/photos?_limit=10')
for (const photo of photos) {

    const html = $('#slide').html()
        .replace('__url__', photo.url)
        .replace('__title__', photo.title)

    slider.slick('slickAdd', html);

}

slider.on('beforeChange', function (event) {

    })
    let card = $('.card')
    let modal = $('#modal')
    let modalContent= $('.modal-content')
    let closing = $('.close')
    card.each(function () {
        $(this).click(function () {
            modal.css("display", "block")
            $(this.childNodes[1]).clone().appendTo(modalContent)
            console.log($(this.childNodes[3]).html())
            $(this.childNodes[3]).clone().appendTo(modalContent)
        })
    })
    closing.click(function () {
        modal.css("display", "none")
        modalContent.empty()
    })
});